import React from 'react';
import styled from '@emotion/styled';
import Zoom from 'react-reveal/Zoom';
import LightSpeed from 'react-reveal/LightSpeed';
import Pulse from 'react-reveal/Pulse';
import '../styles/style.css';

const HeaderImg =
  'https://images.ctfassets.net/srdtyetm50fi/6PyT1wF3vJ8YJTiksDYNa/fb767a978baaf07c6459fc3aea44c13b/about.jpg';


const Text_ = styled.div`
  color: ${props => props.theme.colors.white.base};
  z-index: 0;
  position: absolute;
  top: 35%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  flex-direction: column;
  text-align: center;
  width: 100%;
  max-width: ${props => props.theme.layout.base};
  padding: 0 2rem;
  margin-bottom: 3rem;
  align-items: center;
`;

const HeaderText = styled.h1`
  color: #fff !important;
  font-family: 'Great Vibes', cursive;
  font-size: 3rem;
`;

const AboutHeader = ({ title, cover, subtitle }) => {
  const HeaderContainer = styled.div`
    height: 70vh;
    background-image: linear-gradient(rgba(0, 0, 0, 0.65),rgba(0, 0, 0, 0.65)),
      url(${HeaderImg});
    background-size: cover;
    background-attachment: fixed;
  `;
  return (
    <HeaderContainer>
      <Text_>
        <LightSpeed left>
          <div
            style={{
              border: '1px solid white',
              width: '40%',
              marginBottom: '1.3em',
            }}
          >
            {' '}
          </div>
        </LightSpeed>
        <Pulse>
          <HeaderText>{title}</HeaderText>
        </Pulse>
        <LightSpeed right>
          <div
            style={{
              border: '1px solid white',
              width: '40%',
              marginBottom: '1.3em',
            }}
          >
            {' '}
          </div>
        </LightSpeed>
        <Zoom bottom cascade>
          {subtitle}
        </Zoom>
        <section id="section07" className="demo">
          <span></span>
          <span></span>
          <span></span>
        </section>
      </Text_>
    </HeaderContainer>
  );
};

export default AboutHeader;

AboutHeader.defaultProps = {
  children: false,
  cover: false,
  date: false,
  title: false,
};
