import React from 'react';
import styled from '@emotion/styled';
import Zoom from 'react-reveal/Zoom';
import LightSpeed from 'react-reveal/LightSpeed';
import Pulse from 'react-reveal/Pulse';
import Img from 'gatsby-image';
import '../styles/style.css';

const HeaderImg =
  'https://images.ctfassets.net/srdtyetm50fi/2j1fq8y7UmRBUsErkucW33/98c95c2f7197a0c94f3eb9bc4f539710/header-back.jpg';

const Wrapper = styled.header`
  -webkit-clip-path: polygon(100% 0, 0 0, 0 70%, 50% 100%, 100% 70%);
  clip-path: polygon(100% 0, 0 0, 0 70%, 50% 100%, 100% 70%);
  @media (max-width: ${props => props.theme.breakpoints.s}) {
    -webkit-clip-path: polygon(100% 0, 0 0, 0 90%, 50% 100%, 100% 90%);
    clip-path: polygon(100% 0, 0 0, 0 90%, 50% 100%, 100% 90%);
  }
  background: ${props => props.theme.gradient.rightToLeft};
  height: 300px;
  @media (max-width: ${props => props.theme.breakpoints.m}) {
    height: 300px;
  }
  @media (max-width: ${props => props.theme.breakpoints.s}) {
    height: 275px;
  }
  position: relative;
  overflow: hidden;
`;

const Text = styled.div`
  color: ${props => props.theme.colors.white.base};
  z-index: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  flex-direction: column;
  text-align: center;
  width: 100%;
  max-width: ${props => props.theme.layout.base};
  padding: 0 2rem;
  margin-bottom: 3rem;
  align-items: center;
`;

const Subtitle = styled.p`
  max-width: 650px;
  color: ${props => props.theme.colors.white.light};
`;

const HeaderText = styled.h1`
  color: #fff !important;
  font-family: 'Great Vibes', cursive;
  font-size: 8rem;
`;

const Header = ({ title, cover, subtitle }) => {
  const HeaderContainer = styled.div`
    height: 100vh;
    background-image: linear-gradient(rgba(0, 0, 0, 0.65), rgba(0, 0, 0, 0.65)),
      url(${cover ? cover : HeaderImg});
    background-size: cover;
  `;
  return (
    <HeaderContainer>
      {/* <Img fluid={cover || {} || [] || ''} /> */}

      <Text>
        <LightSpeed left>
          <div
            style={{
              border: '1px solid white',
              width: '40%',
              marginBottom: '1.3em',
            }}
          >
            {' '}
          </div>
        </LightSpeed>
        <Pulse>
          <HeaderText>{title}</HeaderText>
        </Pulse>
        <LightSpeed right>
          <div
            style={{
              border: '1px solid white',
              width: '40%',
              marginBottom: '1.3em',
            }}
          >
            {' '}
          </div>
        </LightSpeed>
        <Zoom bottom cascade>
          {subtitle}
        </Zoom>
        {/* {children && <Subtitle>{children}</Subtitle>} */}
        <section id="section07" className="demo">
          <span></span>
          <span></span>
          <span></span>
        </section>
      </Text>

      {/* </Wrapper> */}
    </HeaderContainer>
  );
};

export default Header;

Header.defaultProps = {
  children: false,
  // cover: false,
  date: false,
  title: false,
};
