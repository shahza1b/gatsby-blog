import React from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import { Layout, Container } from 'layouts';
import AboutImg from '../../static/images/about.jpg';
import { Fade } from 'react-reveal';
import { SocialIcon } from 'react-social-icons';
import AboutHeader from './../components/AboutHeader';

const title =
  '“ Our time is limited, so don’t waste it living someone else’s life.”';


const About = center => (
  <Layout>
    <Helmet title={'About | Shahzaib A.'} />
    <AboutHeader title={title} cover={AboutImg} subtitle="– Steve Jobs" />
    <Container>
      <Fade bottom>
        <h3>Full Time Web Developer - Part Time Open Source Contributor</h3>
      </Fade>

      <Fade bottom cascade>
        <div>
          <blockquote>
            <p>
              <strong>
                You’re at the&nbsp;right place&nbsp;if you face these
                problem(s):
              </strong>
            </p>
            <ul>
              <li>
                You&nbsp;have&nbsp;a big dream to build&nbsp;a blog biz, but
                no&nbsp;concrete results&nbsp;to show for it
              </li>
              <li>
                You are afraid of falling for&nbsp;false promises and fluff in
                an industry of marketers who do not provide value
              </li>
              <li>
                You are&nbsp;tired of “investing” in courses that do not really
                work
              </li>
              <li>
                You&nbsp;are&nbsp;lost and don’t know how to start or continue
              </li>
              <li>
                A simple task of creating an online blog business has been made
                real complicated
              </li>
            </ul>
          </blockquote>
          <SocialIcon url="https://www.facebook.com/Shahza1bb" />
          <SocialIcon url="https://twitter.com/Shahza1bb" />
          <SocialIcon url="https://www.instagram.com/shahxaib_/" />
          <SocialIcon network="whatsapp" url="https://wa.me/+923044430260" />
        </div>
      </Fade>
    </Container>
  </Layout>
);

export default About;

About.propTypes = {
  center: PropTypes.object,
};
