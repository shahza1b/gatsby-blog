import React from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import { Header } from 'components';
import { Layout, Container } from 'layouts';
import '../styles/style.css';

const About = center => (
  <Layout>
    <Helmet title={'Get In Touch | Shahzaib A.'} />
    <Header title="Get In Touch"/>
    {/* <Container center={center}> */}
      <div className="contact-form">
      <form name="contact" method="post" data-netlify="true" data-netlify-honeypot="bot-field">
      <input type="hidden" name="form-name" value="contact" />
          <input type="text" placeholder="Full Name" name="name" required />
          <input type="email" placeholder="Email" name="email" required />
          <textarea
            name="message"
            placeholder="Messages..."
            cols="30"
            rows="10"
          ></textarea>
           <div data-netlify-recaptcha="true"></div>
          <button type="submit">Submit</button>
        </form>
      </div>
    {/* </Container> */}
  </Layout>
);

export default About;

About.propTypes = {
  center: PropTypes.object,
};
